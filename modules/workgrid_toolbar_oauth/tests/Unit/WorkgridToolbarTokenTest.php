<?php

namespace Drupal\Tests\workgrid_toolbar_oauth\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\Config\Config;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\workgrid_toolbar_oauth\Controller\WorkgridToolbarOauthController;
use Drupal\key\KeyRepositoryInterface;
use Drupal\key\Entity\Key;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;

/**
 * Test description.
 *
 * @group workgrid_toolbar
 * @coversDefaultClass \Drupal\commerce\PurchasableEntityTypeRepository
 * @group commerce
 */
class WorkgridToolbarTokenTest extends UnitTestCase {

  /**
   * Configuration Interface mock object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactoryMock;

  /**
   * Stores the keyrepository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  private $keyrepository;

  /**
   * Stores the keyrepository.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * Config mock object.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $configMock;

  /**
   * Workgrid Toolbar settins form mock object.
   *
   * @var \Drupal\workgrid_toolbar_oauth\Controller\WorkgridToolbarOauthController
   */
  private $workgridToolbar;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->keyrepository = $this->prophesize(KeyRepositoryInterface::CLASS);
    $key = $this->prophesize(Key::CLASS);
    $key->getKeyValue()->willReturn(Json::encode([
      "client_id" => "client_id value",
      "client_secret" => "client_secret value",
    ]));
    $this->keyrepository->getKey("test")->willReturn($key->reveal());

    $this->client = $this->prophesize(Client::CLASS);

    // Create mock to return config that will be used in the code under test.
    $this->configMock = $this->prophesize(Config::class);

    /*
     * When the get method of the config is called with the
     *   parameter 'custom_property', the array [
     *    'label' => 'Discounts'
     *   ] will be returned.
     */
    $this->configMock->get('workgridCredentials')->willReturn([
      "client_id" => "client_id value",
      "client_secret" => "client_secret value",
    ]);

    $this->configMock->get('tokenExpiration')->willReturn([
      'label' => 'Token Expiration',
    ]);

    $this->configMock->get('spaceId')->willReturn([
      'label' => 'Space Id',
    ]);

    $this->configMock->get('companyCode')->willReturn([
      'label' => 'Company Code',
    ]);

    $this->configFactoryMock = $this->prophesize(ConfigFactoryInterface::class);
    $this->configFactoryMock
      ->getEditable('workgrid_toolbar_oauth.settings')
      ->willReturn($this->configMock);

    $this->configFactoryMock->get('workgrid_toolbar_oauth.settings')->willReturn($this->configMock->reveal());
    $this->keyrepository->getKey($this->configFactoryMock->get('workgridCredentials'))->willReturn($key->reveal());

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('key.repository', $this->keyrepository);
    $container->set('http_client', $this->client);
    $container->set('config.factory', $this->configFactoryMock);

    \Drupal::setContainer($container);
    $container->set('string_translation', self::getStringTranslationStub());
  }

  /**
   * Test callback.
   */
  public function testAuthorization() : void {
    $this->client->post("https://test.com", [
      'verify' => TRUE,
      'form_params' => [],
    ])->willReturn("{}");

    $this->workgridToolbar = new WorkgridToolbarOauthController(
      $this->keyrepository->reveal(),
      $this->client->reveal(),
      $this->configFactoryMock->reveal()
    );
    $result = $this->workgridToolbar->build();
    $this->assertSame($result->getContent(), '{"message":"Client Id and Client Secret is not set"}');
  }

  /**
   * Test callback.
   */
  public function testAuthorizationResponse() : void {

    $this->client->post("https://auth.test.workgrid.com/oauth2/token", [
      "verify" => TRUE,
      "form_params" => [
        "client_id" => "client_id value",
        "client_secret" => "client_secret value",
        "redirect_uri" => "",
        "grant_type" => "client_credentials",
        "authorization_uri" => "https://auth.test.workgrid.com/oauth2/token",
        "token_uri" => "https://auth.test.workgrid.com/oauth2/token",
        "scopes" => "com.workgrid.api/tokens.all",
        "access_token_url" => "https://auth.test.workgrid.com/oauth2/token",
        "resource_owner_uri" => "",
      ],
      "headers" => [
        "Content-type" => "application/x-www-form-urlencoded",
      ],
    ])->willReturn();

    $this->workgridToolbar = new WorkgridToolbarOauthController(
      $this->keyrepository->reveal(),
      $this->client->reveal(),
      $this->configFactoryMock->reveal()
    );
    $result = $this->workgridToolbar->build();
    $this->assertSame($result->getContent(), '{"message":"Authorization Token Response is empty"}');
  }

}
