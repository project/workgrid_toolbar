INTRODUCTION
This module provides Workgrid customers an easy and 
repeatable way to add the Workgrid Toolbar to their existing Drupal sites. 
The module provides admins a configuration form to specify your Workgrid
organization, space, and toolbar authorization settings. 
Toolbar will be added as a custom block to the content of your Drupal site. 

Standard block configuration settings as well as optional top and 
bottom margin settings available within the block configuration.

A sub-module is provided for those Workgrid customers using oauth credentials 
for toolbar authentication rather than private keys.

You must be an existing Workgrid customer and have access to your tenant name 
and space id in order to properly configure the toolbar module. 
For additional information on Workgrid, please visit us at 
[workgrid]: https://www.workgrid.com

PROJECT SITE 
https://www.drupal.org/project/workgrid_toolbar

REQUIREMENTS
Drupal Core 

INSTALLATION
Install this module as you would any other contributed Drupal module. 
Visit https://www.drupal.org/node/1897420 for further information.
The configuration page is at admin/config/system/workgrid-toolbar, 
where you can configure the workgrid tenant and space level configurations 
for your site.

CONFIGURATION
1. Navigate to Administation > Extend and enable the module
2. Navigate to Administation > Configuration > System > Workgrid Toolbar to 
administer workgrid tenant and space level configurations, 
including organization name, space ID, and auth token endpoint.
3. Navigate to Structure > Block Layout to add a new Workgrid Toolbar block and 
configure roles and styling for the Toolbar.

SUB MODULES
workgrid-toolbar-oauth

REQUIREMENTS
Drupal Core, Key, OAuth2_Client

INSTALLATION
The configuration page is at admin/config/system/workgrid-toolbar-oauth

CONFIGURATION
1. Navigate to Administation > Extend and enable the module
2. Navigate to Administation > Configuration > System > Workgrid Toolbar OAuth 
to administer workgrid tenant and space level configurations, 
including organization name, space ID, and credentials setup.
3. Navigate to Structure > Block Layout to add and configure 
a new Workgrid Toolbar block.
