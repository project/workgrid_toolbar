<?php

namespace Drupal\Tests\workgrid_toolbar\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Workgrid Toolbar tools functionality.
 *
 * @group workgrid_toolbar
 */
class WorkgridToolbarVisibilityTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'block',
    'workgrid_toolbar',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test user with permission to access the administrative toolbar.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->drupalPlaceBlock(
      'workgrid_toolbar',
      [
        'region' => 'help',
        'marginbottom' => '20px',
        'margintop' => '20px',
        'tabs' => [],
      ]
    );
  }

  /**
   * Tests authenticated user can see workgrid toolbar wrapper.
   */
  public function testWorkgridAuthenticatedUser() {
    $this->user = $this->drupalCreateUser();
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    $html = $this->getSession()->getPage()->getHtml();
    $this->assertFalse(strpos($html, 'app.workgrid.com'));
  }

  /**
   * Tests anonymous user cann't see workgrid toolbar wrapper.
   */
  public function testWorkgridAnonymousUser() {
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    $html = $this->getSession()->getPage()->getHtml();
    $this->assertFalse(strpos($html, 'app.workgrid.com'));
  }

}
